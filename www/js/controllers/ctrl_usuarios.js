angular.module('starter.ctrl_usuarios', [])

.controller('usuarios', function($scope, $location, $cordovaSQLite, $timeout, $ionicLoading){

	$scope.usuarios = [];

	// Inicia Loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	$timeout( function(){
		$cordovaSQLite.execute(db, "SELECT * FROM USUARIOS", []).then(function(result) {
			if (result.rows.length > 0) {
				for(var i=0; i<result.rows.length; i++){
		            $scope.usuarios.push(result.rows.item(i));
		        }
			}
			$ionicLoading.hide();
		}, function(error){
			console.log('Erro: ', error);
		});
	}, 1000);

	$scope.fnc_usuarios_add = function () {
		$location.path('app/usuarios_add');
	};
})


.controller('usuarios_add', function($scope, $ionicPopup, $ionicLoading, $cordovaSQLite, $timeout, $location, $stateParams){


	$scope.usuario = {
		NOME: '',
		EMAIL: '',
		SENHA: ''
	}

	$scope.id = $stateParams.id;

	if ($scope.id != undefined) {

		// Inicia Loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		$timeout( function(){
			$cordovaSQLite.execute(db, "SELECT * FROM USUARIOS WHERE ID = ?", [$scope.id]).then(function(result) {
				console.log(result);
				if (result.rows.length > 0) {
					$scope.usuario = result.rows.item(0);
				}
				$ionicLoading.hide();
			}, function(error){
				console.log('Erro: ', error);
			});
		}, 1000);
	}

	$scope.salvar_usuario = function () {

		// Valida o nome do usuario
		if ($scope.usuario.NOME == '') {
			$ionicPopup.alert({
                title: "Atenção",
                template: "Preencha o Nome"
            });
			return false;
		}
		// Valida o e-mail do Usuario
		if ($scope.usuario.EMAIL == '') {
			$ionicPopup.alert({
                title: "Atenção",
                template: "Preencha o Email"
            });
			return false;
		}
		// Valida a senha do Usuario
		if ($scope.usuario.SENHA == '') {
			$ionicPopup.alert({
                title: "Atenção",
                template: "Preencha a Senha"
            });
			return false;
		}

        // Inicia Loader
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

		$sql = "INSERT INTO USUARIOS (NOME, EMAIL, SENHA, AVATAR) VALUES (?, ?, ?, ?);";
		if ($scope.id != undefined) {
			$sql = "UPDATE USUARIOS SET NOME = ?, EMAIL = ?, SENHA = ?, AVATAR = ? WHERE ID = "+$scope.id;
		}
		$cordovaSQLite.execute(db, $sql, [$scope.usuario.NOME, $scope.usuario.EMAIL, $scope.usuario.SENHA, 'img/avatar.png']).then(function(result) {
			$timeout( function(){
                $ionicLoading.hide();
            	$location.path('app/usuarios');
            }, 2000);
        }, function(error){
			$ionicLoading.hide();
            console.log('Erro: ', error);
        });
	};

})
