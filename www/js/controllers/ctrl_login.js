angular.module('starter.ctrl_login', [])

.controller('login', function($scope, $location, $ionicLoading, $timeout, $ionicPopup) {


	$scope.user = {
		email : '',
		password : ''
	}


	$scope.login = function () {

		// Valida se o usuário possui algum valor
		if ($scope.user.email == '') {
			// alert('Preencha seu e-mail');
			$ionicPopup.alert({
                title: "Atenção",
                template: "Preencha seu e-mail"
            });
			return false;
		}
		// Valida se a senha possui algum valor
		if ($scope.user.password == '') {
			// alert('Preencha sua senha');
			$ionicPopup.alert({
                title: "Atenção",
                template: "Preencha sua senha"
            });
			return false;
		}

        // Inicia Loader
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });



		if ($scope.user.email == 'douglas@gmail.com' && $scope.user.password == '123') {
			// alert("Login OK");
			// $ionicPopup.alert({
   //              title: "Atenção",
   //              template: "Login OK"
   //          });

            $timeout( function(){
                $ionicLoading.hide();
            	$location.path('app/inicial');
            }, 2000);
		} else {
			// alert('Erro de Login');

            $timeout( function(){
                $ionicLoading.hide();
                $ionicPopup.alert({
	                title: "Atenção",
	                template: "Erro de Login"
	            });
            }, 2000);
		}
	}
});