// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var db = null;

angular.module('starter', ['ionic', 'ngCordova', 'starter.ctrl_login', 'starter.ctrl_usuarios'])

.run(function($ionicPlatform, $cordovaSQLite) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }

        // Abre uma instância do banco de dados
        if (window.cordova) {
            // Funciona para Android e o IOS
            db = window.sqlitePlugin.openDatabase({name: 'aula.db', iosDatabaseLocation: 'Library'}, success, error);
            function success(msg) {
            // alert("SUCESSO");
            }
            function error(erro) {
            // alert("ERRO");
            }
        } else {
            // Funciona para Navegador
            db = window.openDatabase("aula.db", '1', 'my', 1024 * 1024 * 100); // browser
        }

        // Cria as tabelas no banco de dados
        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS USUARIOS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOME TEXT, EMAIL TEXT, SENHA TEXT, AVATAR TEXT)");
    });
})


.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

    .state('login', {
        cache: false,
        url: '/login',
        abstract: true,
        templateUrl: 'templates/login.html'
    })
    .state('login.acesso', {
        cache: false,
        url: '/acesso',
        views: {
            'menuContent': {
                templateUrl: 'templates/acesso.html'
            }
        }
    })
    .state('app', {
        cache: false,
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html'
    })
    .state('app.inicial', {
        cache: false,
        url: '/inicial',
        views: {
            'menuContent': {
                templateUrl: 'templates/inicial.html'
            }
        }
    })
    .state('app.usuarios', {
        cache: false,
        url: '/usuarios',
        views: {
            'menuContent': {
                templateUrl: 'templates/usuarios.html'
            }
        }
    })
    .state('app.usuarios_add', {
        cache: false,
        url: '/usuarios_add',
        views: {
            'menuContent': {
                templateUrl: 'templates/usuarios_add.html'
            }
        }
    })
    .state('app.usuarios_edt', {
        cache: false,
        url: '/usuarios_edt/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/usuarios_add.html'
            }
        }
    });
    $urlRouterProvider.otherwise('login/acesso');
})
